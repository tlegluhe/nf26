#POur faire marcher fonction, tapper:
# %run TP/question1 EFKI 2003 2004 tmpf

from cassandra.cluster import Cluster
from pyspark import SparkContext
from functools import reduce
from pyspark.sql.functions import avg
import numpy as np
import sys
import matplotlib.pyplot as plt
import itertools
import config

cluster = Cluster(['localhost'])
session = cluster.connect('tlegluhe_metar')
session.default_fetch_size = 100
sc = SparkContext.getOrCreate()

numericindicators = ['tmpf', 'dwpf', 'relh', 'drct', 'sknt', 'p01i', 'alti', 'mslp', 'vsby', 'gust', 'feel', 'ice_accretion_1hr', 'ice_accretion_3hr', 'ice_accretion_6hr', 'peak_wind_gust', 'peak_wind_drct']

if __name__ == "__main__":
	param = "tmpf"
	annee_deb = 2003
	annee_fin = 2012
	point = "EFKI"

	if len(sys.argv) > 1:
		point = sys.argv[1]

	if len(sys.argv) > 2:
		if(int(sys.argv[2]) > 2002 and int(sys.argv[2])	< 2013):
			annee_deb = int(sys.argv[2])

	if len(sys.argv) > 3:
		if(int(sys.argv[3]) > 2002 and int(sys.argv[3]) < 2013 and int(sys.argv[3]) > int(sys.argv[2]) ):
			annee_fin = int(sys.argv[3])

	if len(sys.argv) > 4:
		if(sys.argv[4] in numericindicators):
			param = sys.argv[4]
		else:
			print('You can\'t choose this indicator for this question')

data = []
for annee in range(annee_deb, annee_fin+1):

	requete = """SELECT station, annee, mois, jour, {}
					FROM finlande1 where station = '{}' AND annee = {};
			""".format(param, point, annee)
	
	data = itertools.chain(data, session.execute(requete))
	


D0 = sc.parallelize(data)

D0 = D0.filter(lambda d: (d[4] is not None))


if((annee_fin - annee_deb) <= 2):

	# PREMIER RESULTAT : GRAPHE SUR LA DUREE CHOISIE PAR MOIS
	#Calcul moyenne, min, max et écart type par mois et par année
	D = D0.map( lambda d:((d[1],d[2]), np.array([1,d[4], d[4], d[4], d[4]**2])))
	D = D.reduceByKey( lambda a,b:(a[0]+b[0],a[1]+b[1],min(a[2],b[2]),max(a[3],b[3]), a[4]+b[4]))


	D = D.map(lambda d: ((d[0][0], d[0][1]), d[1][1]/d[1][0], d[1][2], d[1][3],
								np.sqrt(-(d[1][1]/d[1][0])**2+d[1][4]/d[1][0])))

		

	#creation tableau de données					   
	data_month = np.zeros(((annee_fin - annee_deb + 1)* 12))
	data_min = np.zeros(((annee_fin - annee_deb + 1)* 12))
	data_max = np.zeros(((annee_fin - annee_deb + 1)* 12))
	data_sqrt = np.zeros(((annee_fin - annee_deb + 1)* 12))
	legend_month = [None] * ((annee_fin - annee_deb + 1)* 12)


	for row in D.collect():
		data_month[(row[0][0] - annee_deb)*12 + row[0][1]-1] = row[1]
		data_min[(row[0][0] - annee_deb)*12 + row[0][1]-1] = row[2]
		data_max[(row[0][0] - annee_deb)*12 + row[0][1]-1] = row[3]
		data_sqrt[(row[0][0] - annee_deb)*12 + row[0][1]-1] = row[4]
		legend_month[(row[0][0] - annee_deb)*12 + row[0][1]-1] = str(str(row[0][1]) + "/" + str(row[0][0]))


	plt.clf()
	plt.cla()

	x = np.arange((annee_fin - annee_deb + 1)* 12)
	plt.subplot(221)
	plt.bar(x, height= data_month) 
	plt.xticks(x, legend_month, fontsize=5, rotation=90)
	plt.xlabel('')
	plt.ylabel('Moyenne ' + param)
	plt.title('Moyenne par mois pour ' + point)

	x = np.arange((annee_fin - annee_deb + 1)* 12)
	plt.subplot(222)
	plt.bar(x, height= data_min) 
	plt.xticks(x, legend_month, fontsize=5, rotation=90)
	plt.xlabel('')
	plt.ylabel('Min ' + param)
	plt.title('Minimum par mois pour ' + point)

	x = np.arange((annee_fin - annee_deb + 1)* 12)
	plt.subplot(223)
	plt.bar(x, height= data_max) 
	plt.xticks(x, legend_month, fontsize=5, rotation=90)
	plt.xlabel('')
	plt.ylabel('Moyenne ' + param)
	plt.title('Maximum par mois pour '+ point)

	x = np.arange((annee_fin - annee_deb + 1)* 12)
	plt.subplot(224)
	plt.bar(x, height= data_sqrt) 
	plt.xticks(x, legend_month, fontsize=5, rotation=90)
	plt.xlabel('')
	plt.ylabel('Ecart type ' + param)
	plt.title('Ecart type par mois pour ' + point)
	plt.subplots_adjust(wspace=0.5, hspace=0.5)
	plt.savefig("data_general.pdf")
	
	

if((annee_fin - annee_deb) > 2):

	# PREMIER RESULTAT : GRAPHE SUR LA DUREE CHOISIE PAR MOIS
	#Calcul moyenne, min, max et écart type par mois et par année
	D = D0.map( lambda d:((d[1]), np.array([1,d[4], d[4], d[4], d[4]**2])))
	D = D.reduceByKey( lambda a,b:(a[0]+b[0],a[1]+b[1],min(a[2],b[2]),max(a[3],b[3]), a[4]+b[4]))


	D = D.map(lambda d: ((d[0]), d[1][1]/d[1][0], d[1][2], d[1][3],
								np.sqrt(-(d[1][1]/d[1][0])**2+d[1][4]/d[1][0])))

	

	#creation tableau de données					   
	data_month = np.zeros((annee_fin - annee_deb + 1))
	data_min = np.zeros((annee_fin - annee_deb + 1))
	data_max = np.zeros((annee_fin - annee_deb + 1))
	data_sqrt = np.zeros((annee_fin - annee_deb + 1))
	legend_month = [None] * ((annee_fin - annee_deb + 1))


	for row in D.collect():
		data_month[(row[0] - annee_deb)] = row[1]
		data_min[(row[0] - annee_deb)] = row[2]
		data_max[(row[0] - annee_deb)] = row[3]
		data_sqrt[(row[0] - annee_deb)] = row[4]
		legend_month[(row[0] - annee_deb)] = str(row[0])
		

	plt.clf()
	plt.cla()

	x = np.arange((annee_fin - annee_deb + 1))
	plt.subplot(221)
	plt.bar(x, height= data_month) 
	plt.xticks(x, legend_month, fontsize=5, rotation=90)
	plt.xlabel('')
	plt.ylabel('Moyenne ' + param)
	plt.title('Moyenne par an pour ' + point)

	x = np.arange((annee_fin - annee_deb + 1))
	plt.subplot(222)
	plt.bar(x, height= data_min) 
	plt.xticks(x, legend_month, fontsize=5, rotation=90)
	plt.xlabel('')
	plt.ylabel('Min ' + param)
	plt.title('Minimum par an pour ' + point)

	x = np.arange((annee_fin - annee_deb + 1))
	plt.subplot(223)
	plt.bar(x, height= data_max) 
	plt.xticks(x, legend_month, fontsize=5, rotation=90)
	plt.xlabel('')
	plt.ylabel('Moyenne ' + param)
	plt.title('Maximum par an pour '+ point)

	x = np.arange((annee_fin - annee_deb + 1))
	plt.subplot(224)
	plt.bar(x, height= data_sqrt) 
	plt.xticks(x, legend_month, fontsize=5, rotation=90)
	plt.xlabel('')
	plt.ylabel('Ecart type ' + param)
	plt.title('Ecart type par an pour ' + point)
	plt.subplots_adjust(wspace=0.5, hspace=0.5)
	plt.savefig("data_general_an.pdf")	
	



#traitement par saison sur toutes les années:

D2 = D0.map( lambda d:(((d[2]-1)//3), np.array([1,d[4], d[4], d[4], d[4]**2])))
D2 = D2.reduceByKey( lambda a,b:(a[0]+b[0],a[1]+b[1],min(a[2],b[2]),max(a[3],b[3]), a[4]+b[4]))


D2 = D2.map(lambda d: ((d[0]), d[1][1]/d[1][0], d[1][2], d[1][3],
							   np.sqrt(-(d[1][1]/d[1][0])**2+d[1][4]/d[1][0])))

data2_moy = np.zeros(4)
data2_min = np.zeros(4)
data2_max = np.zeros(4)
data2_sqrt = np.zeros(4)
legend_saison = ["Printemps","Ete","Automne","Hiver"]

for row in D2.collect():
	data2_moy[(row[0])] = row[1]
	data2_min[(row[0])] = row[2]
	data2_max[(row[0])] = row[3]
	data2_sqrt[(row[0])] = row[4]

plt.clf()
plt.cla()

x = np.arange(4)
plt.subplot(221)
plt.bar(x, height= data2_moy) 
plt.xticks(x, legend_saison, fontsize=5, rotation=90)
plt.xlabel('')
plt.ylabel('Moyenne ' + param)
plt.title('Moyenne par saison pour ' + point)

x = np.arange(4)
plt.subplot(222)
plt.bar(x, height= data2_min) 
plt.xticks(x, legend_saison, fontsize=5, rotation=90)
plt.xlabel('')
plt.ylabel('Min ' + param)
plt.title('Minimum par saison pour ' + point)

x = np.arange(4)
plt.subplot(223)
plt.bar(x, height= data2_max) 
plt.xticks(x, legend_saison, fontsize=5, rotation=90)
plt.xlabel('')
plt.ylabel('Moyenne ' + param)
plt.title('Maximum par saison pour '+ point)

x = np.arange(4)
plt.subplot(224)
plt.bar(x, height= data2_sqrt) 
plt.xticks(x, legend_saison, fontsize=5, rotation=90)
plt.xlabel('')
plt.ylabel('Ecart type ' + param)
plt.title('Ecart type par saison pour ' + point)
plt.subplots_adjust(wspace=0.5, hspace=0.5)
plt.savefig("data_saison.pdf")

	