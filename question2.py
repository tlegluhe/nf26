#POur faire marcher fonction, tapper:
# %run TP/question2 tmpf 2010 1 1 0 0

from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
from cassandra.cluster import Cluster
from pyspark import SparkContext
from functools import reduce
from pyspark.sql.functions import avg
import sys
import config

plt.cla()
plt.clf()
cluster = Cluster(['localhost'])
session = cluster.connect('tlegluhe_metar')
sc = SparkContext.getOrCreate()



if __name__ == "__main__":
	param = "tmpf"
	annee=2003
	mois=1
	jour =1
	heure=0
	minute=20
	
	if len(sys.argv) > 1:	
		param = sys.argv[1]
	
	if len(sys.argv) > 2:
		if(int(sys.argv[2]) > 2002 and int(sys.argv[2]) < 2013):
			annee= int(sys.argv[2])
		
	if len(sys.argv) > 3:	
		if(int(sys.argv[3]) > 0 and int(sys.argv[3]) < 13):
			mois= int(sys.argv[3])
			
	if len(sys.argv) > 4:	
		if(int(sys.argv[4]) > 0 and int(sys.argv[4]) < 31 ):
			jour= int(sys.argv[4])		
	
	if len(sys.argv) > 5:	
		if(int(sys.argv[5]) >= 0 and int(sys.argv[5]) < 24 ):
			heure= int(sys.argv[5])		
	
	if len(sys.argv) > 6:	
		if(int(sys.argv[6]) >= 0 or int(sys.argv[6]) < 10 ):
			minute = 0
		elif(int(sys.argv[6]) >= 10 or int(sys.argv[6]) < 35 ):
			minute = 20
		elif(int(sys.argv[6]) >= 35 and int(sys.argv[6]) < 60 ):
			minute = 50
	
		
		


	
requete = """SELECT lat,lon,station,{}
				FROM finlande2 where annee = {} AND mois = {} AND jour= {} AND heure = {} AND min = {}
				LIMIT 5000
				;
		  """.format(param, annee, mois, jour, heure, minute)
data = session.execute(requete)


lats = []
lons = []
values = []

for row in data:
	if (row[0]!=None) & (row[0]>=58) & (row[0]<=72) & (row[1]!=None) & (row[1]>=20) & (row[1]<=32) & (row[3] != None):
		lats.append(row[0])
		lons.append(row[1])
		if(param in numericindicators):
			values.append(round(row[3], 2))
		else:
			values.append(row[3])

 
map = Basemap(projection='merc', lat_0 = 65, lon_0 = 26,
	resolution = 'h', area_thresh = 0.1,
	llcrnrlon=20, llcrnrlat=59,
	urcrnrlon=32, urcrnrlat=71)
 
 
map.drawcountries()
map.drawmapboundary(fill_color='#46bcec')
map.fillcontinents(color = 'white',lake_color='#46bcec')
 
x,y = map(lons, lats)
map.plot(x, y, 'bo', markersize=3)

labels = values

 
for label, xpt, ypt in zip(labels, x, y):
	plt.text(xpt+20000, ypt+20000, label, fontsize=6)
plt.title(''+ param + ' à ' + str(heure) +'h'+ str(minute) + ' le ' + str(jour) +'/'+ str(mois) +'/'+ str(annee) + '')
plt.savefig("carte_finlande.pdf")
print("Done ! Check the file carte_finlande.pdf at the root of this project")
