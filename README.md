# NF26

## Sujet
### Objectif
Construire un outil répondants aux questions suivantes :
- Pour un point donné de l’espace, je veux pouvoir avoir un historique du
passé, avec des courbes adaptés. Je vous pouvoir mettre en évidence la
saisonnalité et les écarts à la saisonnalité.
- À un instant donné je veux pouvoir obtenir une carte me représentant
n’importe quel indicateur.
- Pour une période de temps donnée, je veux pouvoir obtenir clusteriser
l’espace, et représenter cette clusterisation.

## Données

Les données utilisées dans se projet sont tirées du Système Automatisé d’Observation en Surface (https://mesonet.agron.iastate.edu/request/download.phtml) au format METAR. Elles concernent la Finlande de l'année 2003 à 2012.

## Stockage
Pour charger les données dans une base de données cql, vous pouvez utiliser le script loader.py qui va charger 200 000 données dans les trois tables correspondant aux trois questions.  
Vous pouvez retrouver la structure des tables dans le fichiers requêtes.cql.  
Sinon, la totalité des données est chargée dans les tables finlande1, finlande2 et finlande3 du keyspace tlegluhe_metar.  

## Executions des fonctions  
Pour répondre aux questions que le sujet pose, il suffit d'exécuter le fichier python correspondant (`%run question1.py` par exemple).  
Il est possible de fournir des arguments pour personnaliser la question.  
La première question prend en arguments une station, une année de début, une année de fin et un nom d'indicateur.  
Par défaut ces paramètres ont pour valeur 'EKFI', 2003, 2012 et 'tmpf'.  
Les valeurs que peut prendre l'indicateur pour cette question sont 
'tmpf', 'dwpf', 'relh', 'drct', 'sknt', 'p01i', 'alti', 'mslp', 'vsby', 'gust', 'feel', 'ice_accretion_1hr', 'ice_accretion_3hr', 'ice_accretion_6hr', 'peak_wind_gust', 'peak_wind_drct'  
Pour savoir ce que signifient ces indicateurs, se référer au lien https://mesonet.agron.iastate.edu/request/download.phtml  
Un exemple d'exécution est `%run question1 EFKI 2003 2004 tmpf`  

La seconde question prend en paramètres un indicateur, une année, un mois, un jour, une heure et une minute.  
Par défaut ces paramètres ont pour valeur 'tmpf', 2010, 1, 1, 0, 0  
Un exemple d'exécution est `%run question2 tmpf 2010 1 1 0 0`  