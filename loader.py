import csv
import re
from cassandra.cluster import Cluster

# CREATE KEYSPACE tlegluhe_metar  WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 2};
# USE tlegluhe_metar;


cluster = Cluster(['localhost'])
session = cluster.connect('tlegluhe_metar')
csvfilename = 'TP/asos.txt'
limit = 200000

def loadata(filename):
	dateparser = re.compile(
	
		"(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+)"
	)
	with open(filename) as f:
		for r in csv.DictReader(f):
			match_date = dateparser.match(r["valid"])
			
			if not match_date:
				continue
			date = match_date.groupdict()
			try:
				data = {}
				data["station"] = r["station"]
				data["date"] = (
					int(date["year"]),
					int(date["month"]),
					int(date["day"]),
					int(date["hour"]),
					int(date["minute"]),
				)
				data["lon"] = float(r["lon"])
				data["lat"] = float(r["lat"])
				
				data["tmpf"] = float(r["tmpf"]) if (r["tmpf"] != 'M') else None
				data["dwpf"] = float(r["dwpf"]) if (r["dwpf"] != 'M') else None
				data["relh"] = float(r["relh"]) if (r["relh"] != 'M') else None
				data["drct"] = float(r["drct"]) if (r["drct"] != 'M') else None
				data["sknt"] = float(r["sknt"]) if (r["sknt"] != 'M') else None
				data["p01i"] = float(r["p01i"]) if (r["p01i"] != 'M') else None
				data["alti"] = float(r["alti"]) if (r["alti"] != 'M') else None
				data["mslp"] = float(r["mslp"]) if (r["mslp"] != 'M') else None
				data["vsby"] = float(r["vsby"]) if (r["vsby"] != 'M') else None
				data["gust"] = float(r["gust"]) if (r["gust"] != 'M') else None
				data["skyc1"] = r["skyc1"] if (r["skyc1"] != 'M') else None
				data["skyc2"] = r["skyc2"] if (r["skyc2"] != 'M') else None
				data["skyc3"] = r["skyc3"] if (r["skyc3"] != 'M') else None
				data["skyc4"] = r["skyc4"] if (r["skyc4"] != 'M') else None
				data["skyl1"] = float(r["skyl1"]) if (r["skyl1"] != 'M') else None
				data["skyl2"] = float(r["skyl2"]) if (r["skyl2"] != 'M') else None
				data["skyl3"] = float(r["skyl3"]) if (r["skyl3"] != 'M') else None
				data["skyl4"] = float(r["skyl4"]) if (r["skyl4"] != 'M') else None
				data["wxcodes"] = r["wxcodes"] if (r["wxcodes"] != 'M') else None
				data["ice_accretion_1hr"] = float(r["ice_accretion_1hr"]) if (r["ice_accretion_1hr"] != 'M') else None
				data["ice_accretion_3hr"] = float(r["ice_accretion_3hr"]) if (r["ice_accretion_3hr"] != 'M') else None
				data["ice_accretion_6hr"] = float(r["ice_accretion_6hr"]) if (r["ice_accretion_6hr"] != 'M') else None
				data["peak_wind_gust"] = float(r["peak_wind_gust"]) if (r["peak_wind_gust"] != 'M') else None
				data["peak_wind_drct"] = float(r["peak_wind_drct"]) if (r["peak_wind_drct"] != 'M') else None
				data["peak_wind_time"] = r["peak_wind_time"] if (r["peak_wind_time"] != 'M') else None
				data["feel"] = float(r["feel"]) if (r["feel"] != 'M') else None
				data["metar"] = r["metar"] if (r["metar"] != 'M') else None
				
				yield data
			except TypeError:
				continue
				
def limiteur(g, limit):
	for i, d in enumerate(g):
		if i >= limit:
			return None
		yield d
		

create_query_1 = '''CREATE TABLE finlande1(station text, annee int, mois int, jour int, heure int, min int, lat float, lon float, tmpf float, dwpf float, relh float, drct float, sknt float, p01i float, alti float, mslp float, vsby float, gust float, skyc1 text, skyc2 text, skyc3 text, skyc4 text, skyl1 float, skyl2 float, skyl3 float, skyl4 float, wxcodes text, ice_accretion_1hr float, ice_accretion_3hr float, ice_accretion_6hr float, peak_wind_gust float, peak_wind_drct float, peak_wind_time text,feel float, metar text, primary key((station, annee), mois, jour, heure, min) );'''
	
create_query_2 = '''CREATE TABLE finlande2(station text, annee int, mois int, jour int, heure int, min int, lat float, lon float, tmpf float, dwpf float, relh float, drct float, sknt float, p01i float, alti float, mslp float, vsby float, gust float, skyc1 text, skyc2 text, skyc3 text, skyc4 text, skyl1 float, skyl2 float, skyl3 float, skyl4 float, wxcodes text, ice_accretion_1hr float, ice_accretion_3hr float, ice_accretion_6hr float, peak_wind_gust float, peak_wind_drct float, peak_wind_time text,feel float, metar text, primary key((annee, mois, jour, heure, min), lat, lon, station ));'''

create_query_3 = '''CREATE TABLE finlande3(station text, annee int, mois int, jour int, heure int, min int, lat float, lon float, tmpf float, dwpf float, relh float, drct float, sknt float, p01i float, alti float, mslp float, vsby float, gust float, skyc1 text, skyc2 text, skyc3 text, skyc4 text, skyl1 float, skyl2 float, skyl3 float, skyl4 float, wxcodes text, ice_accretion_1hr float, ice_accretion_3hr float, ice_accretion_6hr float, peak_wind_gust float, peak_wind_drct float, peak_wind_time text,feel float, metar text, primary key((annee), mois, jour, heure, min));'''

def writecassandra(csvfilename, limit, session, tablename):
	g = loadata(csvfilename)
	data = limiteur(g, limit)
	for r in data:
		t = (

					r["station"],
					
					r["date"][0],
					r["date"][1],
					r["date"][2],
					r["date"][3],
					r["date"][4],
					
					r["lat"],
					r["lon"],
					r["tmpf"],
					r["dwpf"],
					r["relh"],
					r["drct"],
					r["sknt"],
					r["p01i"],
					r["alti"],
					r["mslp"],
					r["vsby"],
					r["gust"],
					r["skyc1"],
					r["skyc2"],
					r["skyc3"],
					r["skyc4"],
					r["skyl1"],
					r["skyl2"],
					r["skyl3"],
					r["skyl4"],
					r["wxcodes"],
					r["ice_accretion_1hr"],
					r["ice_accretion_3hr"],
					r["ice_accretion_6hr"],
					r["peak_wind_gust"],
					r["peak_wind_drct"],
					r["peak_wind_time"],
					r["feel"],
					r["metar"]
					
					)	
		query = """
		INSERT INTO """+tablename+"""(

			station,
			annee,
			mois,
			jour,
			heure,
			min,
			lat,
			lon,
			tmpf,
			dwpf,
			relh,
			drct,
			sknt,
			p01i,
			alti,
			mslp,
			vsby,
			gust,
			skyc1,
			skyc2,
			skyc3,
			skyc4,
			skyl1,
			skyl2,
			skyl3,
			skyl4,
			wxcodes,
			ice_accretion_1hr,
			ice_accretion_3hr,
			ice_accretion_6hr,
			peak_wind_gust,
			peak_wind_drct,
			peak_wind_time,
			feel,
			metar)
		VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
		"""
		session.execute(query, t)
			

session.execute('DROP TABLE IF EXISTS finlande1')
session.execute(create_query_1)	
writecassandra(csvfilename,limit,session, 'finlande1')

session.execute('DROP TABLE IF EXISTS finlande2')
session.execute(create_query_2)	
writecassandra(csvfilename,limit,session,'finlande2')

session.execute('DROP TABLE IF EXISTS finlande3')
session.execute(create_query_3)	
writecassandra(csvfilename,limit,session,'finlande3')