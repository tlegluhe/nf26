#Pour faire marcher cette fonction, tapper:
# %run TP/question3 2009 2010 1 3 tmpf 

from cassandra.cluster import Cluster
import sys
import numpy as np
import glob
from math import sqrt
import random
from pyspark import  SparkContext
from functools import reduce
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cm
import itertools
import config

plt.cla()
plt.clf()

cluster = Cluster(['localhost'])
session = cluster.connect('tlegluhe_metar')
session.default_fetch_size = 100
sc = SparkContext.getOrCreate()


def getIndicatorCluster():

	data = []
	for annee in range(annee_deb, annee_fin+1):
		if( annee == annee_deb):
			requete = """SELECT lat,lon, {}
							FROM finlande3 where annee = {} and mois >= {} ;
					""".format(param, annee, mois_deb)
			
			data = itertools.chain(data, session.execute(requete))
			
		elif( annee == (annee_fin + 1)):
			requete = """SELECT lat,lon, {}
							FROM finlande3 where annee = {} and mois <= {} ;
					""".format(param, annee, mois_fin)
			
			data = itertools.chain(data, session.execute(requete))
			
		else:
			requete = """SELECT lat,lon, {}
							FROM finlande3 where annee = {};
					""".format(param, annee)
			
			data = itertools.chain(data, session.execute(requete))
			
	for dat in data:
			flag = True
			for x in dat:
				if (x == None): flag = False
			if (flag & (dat[2]!=None) & (dat[0]>=58) & (dat[0]<=72) & (dat[1]>=20) & (dat[1]<=32) ):
				yield dat		

def select_random(data, n=2): 
    result = [0] * n
    for index, r in enumerate(data):
        if index < n:
            result[index] = r
        else:
            if random.uniform(0,1) < n/(index+1):
                result[random.randint(0, n-1)] = r

    return result

def dist(r, c):
    n = len(r)
    temp = []
    for i in range(n):
        temp.append(r[i])
    temp2 = []
    for i in range(len(temp)):
        temp2.append(temp[i] - c[i])
    temp2 = [x**2 for x in temp2]
    return sqrt(sum(temp2))
	
def kmeans(k):
	n_conv = 20
	epsilon = 0.01

	def dist_c(c1, c2):
		temp = []
		for i in range(len(c1)):
			temp.append(c1[i] - c2[i])
		temp = [x**2 for x in temp]
		return sqrt(sum(temp))

	def get_values(r):
		c = []
		for i in range(2,len(r)):
			c.append(r[i])
		return tuple(c)

	def sum_v(x,y):
		return tuple([ a+b for a,b in zip(x,y)])
	def mul_v(s,v):
		return tuple([ s*v_i for v_i in v])

	centroids = [get_values(r) for r in select_random(getIndicatorCluster())]
	centroids_count = None
	for i in range(n_conv):
		accu_centroids = [(0,) * nb_param] * k
		accu_count = [0] * k
		for r in getIndicatorCluster():
			dist_centroids = [dist(r[2:], c) for c in centroids]
			centroid_id = dist_centroids.index(min(dist_centroids))
			accu_centroids[centroid_id] = sum_v(accu_centroids[centroid_id], get_values(r))
			accu_count[centroid_id] += 1

		new_centroids = [mul_v(1/n, c) for c, n in zip(accu_centroids, accu_count)]
		diff_centroids = [dist_c(old, new) for old, new in zip(centroids, new_centroids)]
		centroids = new_centroids
		centroids_count = accu_count
		if max(diff_centroids) < epsilon:
			break

	res = {}
	res['centers'] = []
	res['count'] = centroids_count
	for centroid in centroids:
		c = {}
		for index, indicator in enumerate(['tmpf']):
			c[indicator]=centroid[index]
		res['centers'].append(c)
	return res

# END OF FUNCTIONS -------------------
# -------------------------------------
#-------------------------------------	

if __name__ == "__main__":
	param = ''
	nb_param=0
	if len(sys.argv) > 5:
		for parameters in sys.argv[5:]:
			param = parameters
			nb_param=nb_param + 1
	else:
		param = "tmpf"
		nb_param=1
		
	annee_deb=2003
	annee_fin=2012
	mois_deb=1
	mois_fin=12
	
	if len(sys.argv) > 1:
		if(int(sys.argv[1]) > 2002 and int(sys.argv[1])	< 2013):
			annee_deb= int(sys.argv[1])
		
	if len(sys.argv) > 2:	
		if(int(sys.argv[2]) > 2002 and int(sys.argv[2]) < 2013 and int(sys.argv[2]) >= int(sys.argv[1]) ):
			annee_fin= int(sys.argv[2])

	if len(sys.argv) > 4:	
		if(int(sys.argv[3]) > 0 and int(sys.argv[3]) < 13 and int(sys.argv[4]) > 0 and int(sys.argv[4]) < 13 ):
			if((int(sys.argv[2]) > int(sys.argv[1])) or (int(sys.argv[3]) <= int(sys.argv[4]))):
				mois_deb= int(sys.argv[3])
				mois_fin= int(sys.argv[4])	
	print("la")
	
	data = []
	for annee in range(annee_deb, annee_fin+1):
		if( annee == annee_deb):
			requete = """SELECT lat,lon, {}
							FROM finlande3 where annee = {} and mois >= {} ;
					""".format(param, annee, mois_deb)
			
			data = itertools.chain(data, session.execute(requete))
			
		elif( annee == (annee_fin + 1)):
			requete = """SELECT lat,lon, {}
							FROM finlande3 where annee = {} and mois <= {} ;
					""".format(param, annee, mois_fin)
			
			data = itertools.chain(data, session.execute(requete))
			
		else:
			requete = """SELECT lat,lon,{}
							FROM finlande3 where annee = {};
					""".format(param, annee)
			
			data = itertools.chain(data, session.execute(requete))

	D0 = sc.parallelize(data)
	
	if(nb_param == 1):
		D0 = D0.filter( lambda d: (d[2] is not None))
		#Avec Ecart Type
		#D0 = D0.map(lambda d: ((d[0], d[1]), np.array([1,d[3],d[4], d[3]**2,d[4]**2])))
		#D0 = D0.reduceByKey(lambda a, b: (a[0]+b[0], a[1]+b[1], a[2]+b[2], a[3]+b[3], a[4]+b[4]))
		#D0 = D0.map(lambda d: ((d[0][0], d[0][1]), d[1][1]/d[1][0], d[1][2]/d[1][0],np.sqrt(-(d[1][1]/d[1][0])**2+d[1][3]/d[1][0]),np.sqrt(-(d[1][2]/d[1][0])**2+d[1][4]/d[1][0])))
		D0 = D0.map(lambda d: ((d[0], d[1]), np.array([1,d[2]])))
		D0 = D0.reduceByKey(lambda a, b: (a[0]+b[0], a[1]+b[1]))
		D0 = D0.map(lambda d: ((d[0][0], d[0][1]), d[1][1]/d[1][0]))

		
		raw_data = {}
		for row in D0.collect():
			if(row[0][0], row[0][1]) not in raw_data:
				raw_data[(row[0][0], row[0][1])] = row[1]

		
		centroids_info = kmeans(2)
		print(centroids_info)
		centroids = [[v for k, v in c.items()] for c in centroids_info["centers"]]
		print(centroids)
		cluster_data = {}
		for i in range(1,1):
			for k, v in raw_data.items():
				dist_centroids = [dist(v[i, :], c) for c in centroids]
				centroid_id = dist_centroids.index(min(dist_centroids))
				if k not in cluster_data:
					cluster_data[k] = np.empty((1,1))
				cluster_data[k][i] = centroid_id


		colors = cm.rainbow(np.linspace(0, 1, len(centroids)))
		fig, ax_arr = plt.subplots(1, sharey=True)
		fig.set_size_inches(18.5, 10.5)
			
		map = Basemap(projection='merc', lat_0 = 65, lon_0 = 26,
				resolution = 'h', area_thresh = 0.1,
				llcrnrlon=20, llcrnrlat=59,
				urcrnrlon=32, urcrnrlat=71)
			
			
		map.drawcountries()
		map.drawmapboundary(fill_color='#46bcec')
		map.fillcontinents(color = 'white',lake_color='#46bcec')
		for k, v in cluster_data.items():
			#x, y = m(k[0], k[1])
				m.scatter(k[1], k[0], marker='o', color=colors[int(v[1, 0])], latlon=True)





